import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
void main() {
  runApp(MaterialApp(
    title: "weather APP",
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  var temp;
  var  description;
  var corrently;
  var humidity;
  var speed;
  var name;

  Future getweather() async{
    // final response = await http.get(Uri.https('http://api.openweathermap.org/data/2.5/weather?q=london&appid=746d932e7b6a090118b432abe57c30dd', 'albums/1'));

    http.Response response= await http.get("http://api.openweathermap.org/data/2.5/weather?q=london&appid=746d932e7b6a090118b432abe57c30dd&units=metric");
    var result = jsonDecode(response.body);
    print(response.body);
    setState(() {
      this.temp=result['main']['temp'];
      this.description=result['weather'][0]['description'];
      this.corrently=result["weather"][0]["main"];
      this.humidity=result['main']['humidity'];
      this.speed=result['wind']['speed'];
      // this.name=result["name"];
    });


}


  @override
  void initState() {
    super.initState();
    this.getweather();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("weather City"),
      // ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            color: Colors.red,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(
                    "Weather",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Text(
                    temp!=null ?temp.toString() +"\u0000":"loading...",

                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.w600)),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(
                    corrently !=null ? corrently.toString()+"\u0000":"loading...",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  ListTile(
                    leading: FaIcon(FontAwesomeIcons.thermometerHalf),
                    title: Text("tempretsher"),
                    trailing: Text(temp !=null ?temp.toString()+"\u0000":"loading..."),
                  ),
                  ListTile(
                    leading: FaIcon(FontAwesomeIcons.cloud),
                    title: Text("clud"),
                    trailing: Text(description!=null ?description+"\u0000":"loading..."),
                  ),
                  ListTile(
                    leading:Icon(Icons.wb_sunny),
                    title: Text("sun"),
                    trailing: Text(humidity!=null ?humidity.toString()+"\u0000":"loading..."),
                  ),
                  ListTile(
                    leading: FaIcon(FontAwesomeIcons.wind),
                    title: Text("wind"),
                    trailing: Text(speed!=null ?speed.toString()+"\u0000":"loading..."),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
